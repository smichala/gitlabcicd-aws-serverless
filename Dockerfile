#Use node v14.15.4 until issue is resolved. https://github.com/serverless/serverless/issues/8772#issuecomment-761767227
FROM node:14.15.4

WORKDIR /usr/local

RUN apt-get update

# Install python 3.8
RUN apt-get install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev -y
RUN wget https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tgz
RUN tar -xf Python-3.8.0.tgz
WORKDIR ./Python-3.8.0
RUN ./configure
RUN make -j 8
RUN make altinstall
WORKDIR /usr/local

#RUN apt-get install python -y
#RUN apt-get install python-pip -y
RUN apt-get install python3-pip -y
#RUN pip --no-cache-dir install cryptography pytest pytest-mock moto simplejson importlib_resources
RUN pip3 --no-cache-dir install cryptography pytest pytest-mock moto simplejson importlib_resources

RUN npm config set prefix /usr/local
RUN npm install -g serverless
RUN npm install serverless-offline --save-dev
RUN npm install serverless-stack-output
RUN npm install --save serverless-python-requirements